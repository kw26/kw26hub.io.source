---
title: Electron 开发总结
---
实习的时候在一家小公司做前端开发, 用Electron做软件客户端。在接触到Electron之前，我一直使用PyQt作为软件客户端解决方案, 但是界面开发起非常痛苦。Electron极大的降低了桌面客户端的开发成本。

## 安装环境

1.nodejs v8.*以上版本 http://nodejs.cn/download/

2.npm 建议使用 cnpm 
```
$ npm install -g cnpm --registry=https://registry.npm.taobao.org
```
3.安装electron
```
$ npm install electron -g
```

4.安装app的依赖
```
$ cd app/
$ npm install
```

## 开发

### About Electron

官方文档 https://electron.atom.io/docs/

### quick start 快速入门 

```
# Clone the Quick Start repository
$ git clone https://github.com/electron/electron-quick-start

# Go into the repository
$ cd electron-quick-start

# Install the dependencies and run
$ npm install && npm start
```

### 全局使用electron 

electron 可以在任意地方被调用

```
$ electron [path]
```

electron窗口出现后可以将electron工程的文件夹直接拖进窗口以运行

### Frameless Window 无边框的窗口

To create a frameless window, you need to set frame to false in BrowserWindow’s options:

在新建窗口对象时添加属性 frame : false

```javascript
const {BrowserWindow} = require('electron')
let win = new BrowserWindow({width: 800, height: 600, frame: false})
win.show()
```

### 进程之间通信

#### 方案1:ipc通信

```javascript
// 主进程
const { ipcMain } = require('electron');

ipcMain.on('send-message',  (e, name) => {

  // edit your code

  // return message to renderer process
  e.sender.send('res-message', `hello ${name}`);
})

// 渲染进程
const ipcRenderer = require('electron').ipcRenderer;

ipcRenderer.send('send-message', 'Tim');

ipcRenderer.on('res-message', (res) => {
    console.log(res)
})

```

#### 方案2:remote共享对象

```javascript
// 在主进程中
global.sharedObject = {
  someProperty: 'default value'
};
// 在第一个页面中
require('remote').getGlobal('sharedObject').someProperty = 'new value';
// 在第二个页面中
console.log(require('remote').getGlobal('sharedObject').someProperty);
```

### 更新中···
