---
title: Flask celery
---

[Celery](http://celeryproject.org) 是一个开源的分布任务队列， 执行某些耗时较长的工作时，可以使用Celery在后台执行，而接口只需返回202即可。

## 安装celery

使用redis作为celery的储存后端
```
$ sudo apt-get install redis-server
```
启动redis
```
redis-server
```
安装celery
``` 
pip install celery
pip install redis
```

## 编写一个task
app.py
``` py
from flask import Flask
from celery import Celery

def make_celery(app):
    celery = Celery(
        app.import_name,
        backend = app.config['CELERY_RESULT_BACKEND'],
        broker = app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def call(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery
    
flask_app = Flask(__name__)
flask_app.config.update(
    CELERY_BROKER_URL = 'redis://localhost:6379',
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
)
celery = make_celery(flask_app)

@celery.task()
def add(a, b):
    return a + b

```

## 启动celery
```
$ celery -A app worker --loglevel=info
```

## 测试
```
$ python3
>>>from app import add
>>>res = add.delay(4, 3)
>>>res.get()
7
```

参考：http://flask.pocoo.org/docs/1.0/patterns/celery/
